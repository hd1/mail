import logging
from java.awt import BorderLayout
from java.lang import System
from java.util import Vector
from javax.mail import Session, Folder
from javax.swing import JFrame, JPanel, JScrollPane, JTable
from javax.swing.table import DefaultTableModel
from org.netbeans.swing.etable import ETable

class ui:
    def __init__(self):
        self.app = JFrame('Mail, by Hasan Diwan (C) 2015')
        self.app.layout = BorderLayout()
        folders = self.initTree()
        model = (Vector(), Vector().addAll(folders))
        tree = ETable(model)
        scroll = JScrollPane(tree)
        self.app.add(scroll, BorderLayout.WEST)
        mainPanel = JPanel(BorderLayout())

        subjects = self.subjects()
        model = DefaultTableModel(Vector(), Vector().addAll(subjects))
        tbl = JTable(model)
        mainPanel.add(JScrollPane(tbl), BorderLayout.NORTH)
        #  Add messsage view pane, updated when tbl's selection changes
        msg = Webbrowser().setContent('')
        mainPanel.add(JScrollPane(msg), BorderLayout.SOUTH)
        self.app.add(mainPanel, BorderLayout.EAST)
        self.app.pack()
        self.app.visible = True
        self.app.defaultCloseOperation = JFrame.EXIT_ON_CLOSE

    def initTree(self):
        props = System.getProperties()
        props.setProperty(u'mail.store.protocol', u'imap.gmail.com')
        session = Session.getDefaultInstance(props)
        store = session.getStore("imaps")
        store.connect(u'imap.gmail.com','hd1@jsc.d8u.us', 'Wonju777')
        folders = []
        [folders.append(fold.getName()) for fold in store.getFolder(u'INBOX').listSubscribed()]
        store.close()
        return folders

    def subjects(self):
        props = System.getProperties()
        props.setProperty(u'mail.store.protocol', u'imap.gmail.com')
        session = Session.getDefaultInstance(props)
        store = session.getStore("imaps")
        store.connect(u'imap.gmail.com','hd1@jsc.d8u.us', 'Wonju777')
        logging.debug('Connected!')
        folder = store.getFolder("INBOX")
        folder.open(Folder.READ_ONLY) # READ_WRITE?
        ret = []
        for m in folder.getMessages():
            logging.debug('{} <=== subject'.format(m.getSubject()))
            ret.append(m.getSubject())
        store.close()
        return ret

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    ui()
