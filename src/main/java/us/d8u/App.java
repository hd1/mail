package us.d8u;
import java.util.Date;
import org.python.core.PyException;
import org.python.util.PythonInterpreter;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try {
            PythonInterpreter interp =
                    new PythonInterpreter();
            interp.execfile(ClassLoader.getSystemResourceAsStream("ui.py"));
        } catch (PyException e) {
            System.err.println(new Date() + ": "+e.getMessage());
            e.printStackTrace(System.err);
		}
	}
}
